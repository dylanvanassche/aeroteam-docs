# aeroteam-docs

This repository hosts the Aeroteam documentation using Gitlab CI on Gitlab pages.

You can access the documentation on the following URL: [https://dylanvanassche.gitlab.io/aeroteam-docs/](https://dylanvanassche.gitlab.io/aeroteam-docs/)

## Updating the documentation

To update the documentation you need to take the following steps:

1. Clone this repository using `git clone`.
2. Add the new HTML pages in your local repository.
3. Commit and push your new documtation using `git commit` and `git push`.
4. Gitlab CI takes care of the rest!